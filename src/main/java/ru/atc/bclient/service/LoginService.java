package ru.atc.bclient.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.atc.bclient.model.entity.User;
import ru.atc.bclient.model.repository.UserRepo;

import java.util.Optional;

@Repository("loginService")
@Transactional
public class LoginService {

    private final UserRepo userRepo;

    @Autowired
    public LoginService(UserRepo userRepo){
        this.userRepo = userRepo;
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserByLoginAndPassword(String login, String password){
        return userRepo.findByUserLoginAndUserPassword(login, password);
    }
}