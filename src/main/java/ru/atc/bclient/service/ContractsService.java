package ru.atc.bclient.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.atc.bclient.model.entity.Contract;
import ru.atc.bclient.model.entity.LegalEntity;
import ru.atc.bclient.model.repository.ContractRepo;
import ru.atc.bclient.web.datamodel.DateFilterModel;

import java.util.*;

@Repository("contactsService")
@Transactional
public class ContractsService {

    private final ContractRepo contractRepo;
    private final LegalEntityService legalEntityService;

    @Autowired
    public ContractsService(ContractRepo contractRepo, LegalEntityService legalEntityService) {
        this.contractRepo = contractRepo;
        this.legalEntityService = legalEntityService;
    }

    @Transactional(readOnly = true)
    public List<Contract> getContractsByEntityLegal(LegalEntity issuer, LegalEntity singer, String sortProperty) {
        return contractRepo.getContractsByIssuerOrSinger(issuer, singer,
                sortProperty != null ? Sort.by(sortProperty) : Sort.unsorted());
    }

    @Transactional(readOnly = true)
    public List<Contract> getContractsByIssuerAndSinger(LegalEntity issuer, LegalEntity singer, String sortProperty) {
        return contractRepo.getContractsByIssuerAndSinger(issuer, singer,
                sortProperty != null ? Sort.by(sortProperty) : Sort.unsorted());
    }

    @Transactional(readOnly = true)
    public List<Contract> getContractsByFilterDate(LegalEntity legalEntity, DateFilterModel date, String sortProperty) {
        return contractRepo.findAll(legalEntity, date.getStartDate(), date.getEndDate(),
                sortProperty != null ? Sort.by(sortProperty) : Sort.unsorted());
    }

    @Transactional(readOnly = true)
    public Contract getContractById(int id) {
        return contractRepo.findById(id).orElse(null);
    }

    @Transactional
    public Contract createContract(Contract contract, LegalEntity issuer) {
        contract.setCurrencyCode("RUB");
        contract.setIssuer(issuer);
        contract.setSinger(legalEntityService.getLegalEntityById(contract.getSinger().getId()));
        return contractRepo.save(contract);
    }
}