package ru.atc.bclient.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import ru.atc.bclient.model.entity.*;
import ru.atc.bclient.model.repository.OperationRepo;
import ru.atc.bclient.model.repository.PaymentOrderRepo;
import ru.atc.bclient.model.repository.PaymentOrderStatusRepo;
import ru.atc.bclient.web.datamodel.DateFilterModel;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Repository("paymentOrderService")
@Transactional
@EnableScheduling
public class PaymentOrderService {

    private volatile boolean flag = true;

    private final PaymentOrderRepo paymentOrderRepo;

    private final PaymentOrderStatusRepo paymentOrderStatusRepo;

    private final OperationRepo operationRepo;

    private final AccountsService accountsService;

    @Autowired
    public PaymentOrderService(PaymentOrderRepo paymentOrderRepo, PaymentOrderStatusRepo paymentOrderStatusRepo, OperationRepo operationRepo, AccountsService accountsService) {
        this.paymentOrderRepo = paymentOrderRepo;
        this.paymentOrderStatusRepo = paymentOrderStatusRepo;
        this.operationRepo = operationRepo;
        this.accountsService = accountsService;
    }

    @Transactional(readOnly = true)
    public PaymentOrderStatus getPaymentOrderStatus(String paymentOrderStatusCode) {
        return paymentOrderStatusRepo.getPaymentOrderStatusByPaymentOrderStatusCode(paymentOrderStatusCode);
    }

    @Transactional(readOnly = true)
    public List<PaymentOrder> getPaymentOrders(LegalEntity legalEntity) {
        return paymentOrderRepo.getPaymentOrdersBySenderOrRecipient(legalEntity);
    }

    @Transactional(readOnly = true)
    public List<PaymentOrder> getPaymentOrderByFilterDate(LegalEntity legalEntity, DateFilterModel date) {
        return paymentOrderRepo.findAllByFilterDate(legalEntity,
                date.getStartDate() == null ? Date.from(LocalDate.of(2000, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()) : date.getStartDate(),
                date.getEndDate() == null ? Date.from(LocalDate.of(2100, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()) : date.getEndDate());
    }

    @Transactional
    public boolean createPayment(PaymentOrder paymentOrder) {
        if (flag) {
            paymentOrderRepo.save(paymentOrder);
            return true;
        }
        return false;
    }

    @Transactional
    public PaymentOrder createPaymentOrder(PaymentOrder paymentOrder){
        return paymentOrderRepo.save(paymentOrder);
    }

    @Transactional
    public void deletePaymentOrder(int paymentId) {
        paymentOrderRepo.deleteById(paymentId);
    }

    public PaymentOrder fillPaymentOrder(PaymentOrder paymentOrder, Contract contract) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String curDate = LocalDate.now().format(fmt);
        try {
            paymentOrder.setPaymentOrderDate(formatter.parse(curDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        paymentOrder.setCurrencyCode("RUB");
        paymentOrder.setContract(contract);
        paymentOrder.setRecipientLegalEntity(contract.getIssuer());
        paymentOrder.setRecipientAccount(accountsService.getAccountById(paymentOrder.getRecipientAccount().getId()));
        paymentOrder.setSenderLegalEntity(contract.getSinger());
        paymentOrder.setSenderAccount(accountsService.getAccountById(paymentOrder.getSenderAccount().getId()));
        paymentOrder.setPaymentOrderStatus(getPaymentOrderStatus("IN_PROGRESS"));
        return paymentOrder;
    }

    @Transactional
    @Scheduled(cron = "0 0 21 * * *")
    public void processing() {
        Thread thread = new Thread(() -> {
            TransactionSynchronizationManager.setActualTransactionActive(true);
            flag = false;
            List<PaymentOrder> paymentOrders = paymentOrderRepo.getPaymentOrders();
            for (PaymentOrder paymentOrder : paymentOrders) {
                BigInteger senderBal = accountsService.getAccountBalance(paymentOrder.getSenderAccount().getId()).getAccountBalanceAmt();
                if (senderBal.compareTo(paymentOrder.getPaymentOrderAmt()) < 0) {
                    paymentOrder.setPaymentOrderStatus(getPaymentOrderStatus("REJECTED"));
                    paymentOrderRepo.save(paymentOrder);
                } else {
                    Operation operation = new Operation();
                    operation.setOperationAmt(paymentOrder.getPaymentOrderAmt());
                    operation.setOperationDate(paymentOrder.getPaymentOrderDate());
                    operation.setDebetAccount(paymentOrder.getRecipientAccount());
                    operation.setCreditAccount(paymentOrder.getSenderAccount());
                    operationRepo.save(operation);
                    accountsService.transaction(paymentOrder.getRecipientAccount(), paymentOrder.getSenderAccount(), paymentOrder.getPaymentOrderAmt());
                    paymentOrder.setPaymentOrderStatus(getPaymentOrderStatus("EXECUTED"));
                    paymentOrderRepo.save(paymentOrder);
                }
            }
            flag = true;
        });
        thread.start();
    }
}