package ru.atc.bclient.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.atc.bclient.model.entity.LegalEntity;
import ru.atc.bclient.model.repository.LegalEntityRepo;

import java.util.List;

@Repository("legalEntityService")
@Transactional
public class LegalEntityService {

    private final LegalEntityRepo legalEntityRepo;

    @Autowired
    public LegalEntityService(LegalEntityRepo legalEntityRepo) {
        this.legalEntityRepo = legalEntityRepo;
    }

    @Transactional(readOnly = true)
    public LegalEntity getLegalEntityById(int id){
        return legalEntityRepo.findById(id).orElse(null);
    }

    @Transactional(readOnly = true)
    public List<LegalEntity> getAllLegalEntity(){
        return (List<LegalEntity>) legalEntityRepo.findAll();
    }
}