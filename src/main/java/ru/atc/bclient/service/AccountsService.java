package ru.atc.bclient.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.atc.bclient.model.entity.*;
import ru.atc.bclient.model.repository.*;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

@Repository("accountsService")
@Transactional
public class AccountsService {

    private final AccountRepo accountRepo;

    private final AccountBalanceRepo accountBalanceRepo;

    private final BankRepo bankRepo;

    private final AccountStatusRepo accountStatusRepo;

    private final LegalEntityService legalEntityService;

    @Autowired
    public AccountsService(AccountRepo accountRepo, AccountBalanceRepo accountBalanceRepo, BankRepo bankRepo, AccountStatusRepo accountStatusRepo, LegalEntityService legalEntityService) {
        this.accountRepo = accountRepo;
        this.accountBalanceRepo = accountBalanceRepo;
        this.bankRepo = bankRepo;
        this.accountStatusRepo = accountStatusRepo;
        this.legalEntityService = legalEntityService;
    }

    @Transactional(readOnly = true)
    public Account getAccountById(int id) {
        return accountRepo.findById(id).orElse(null);
    }

    @Transactional(readOnly = true)
    public List<Account> getAccounts(LegalEntity legalEntity, String sortProperty) {
        List<Account> accountList = accountRepo.getAccountByLegalEntity(legalEntity,
                sortProperty != null ? Sort.by(sortProperty) : Sort.unsorted());
        return setBalance(accountList);
    }


    @Transactional(readOnly = true)
    public List<Account> getAccountsByBank(String bankName, LegalEntity legalEntity, String sortProperty) {
        List<Account> accountList = accountRepo.findAllByBankBankNameAndLegalEntity(bankName, legalEntity,
                sortProperty != null ? Sort.by(sortProperty) : Sort.unsorted());
        return setBalance(accountList);
    }

    @Transactional(readOnly = true)
    public AccountBalance getAccountBalance(int accountId) {
        return accountBalanceRepo.getTopByAccountId(accountId);
    }

    @Transactional(readOnly = true)
    public List<Bank> getBanks() {
        return (List<Bank>) bankRepo.findAll();
    }

    @Transactional(readOnly = true)
    public Bank getBankById(int id) {
        return bankRepo.findById(id).orElse(null);
    }

    @Transactional(readOnly = true)
    public AccountStatus getAccountStatus(String accountStatusCode) {
        return accountStatusRepo.getAccountStatusByAccountStatusCode(accountStatusCode);
    }

    @Transactional()
    public void createNewAccount(Account account) {
        account.setAccountStatus(getAccountStatus("ACTIVE"));
        account.setBank(getBankById(account.getBank().getId()));
        account.setLegalEntity(legalEntityService.getLegalEntityById(account.getLegalEntity().getId()));
        accountRepo.save(account);
        AccountBalance accountBalance = new AccountBalance();
        Timestamp currentDate = new Timestamp(System.currentTimeMillis());
        accountBalance.setAccountBalanceDate(currentDate);
        accountBalance.setAccountBalanceAmt(new BigInteger("0"));
        accountBalance.setAccount(account);
        accountBalanceRepo.save(accountBalance);
    }

    @Transactional()
    public void transaction(Account recipient, Account sender, BigInteger sum) {
        Timestamp currentDate = new Timestamp(System.currentTimeMillis());
        BigInteger bigIntegerSen = getAccountBalance(sender.getId()).getAccountBalanceAmt();
        BigInteger bigIntegerRec = getAccountBalance(recipient.getId()).getAccountBalanceAmt();

        AccountBalance recipientAccBal = new AccountBalance();
        recipientAccBal.setAccount(recipient);
        recipientAccBal.setAccountBalanceDate(currentDate);
        bigIntegerRec = bigIntegerRec.add(sum);
        recipientAccBal.setAccountBalanceAmt(bigIntegerRec);
        accountBalanceRepo.save(recipientAccBal);

        AccountBalance senderAccBal = new AccountBalance();
        senderAccBal.setAccount(sender);
        senderAccBal.setAccountBalanceDate(currentDate);
        bigIntegerSen = bigIntegerSen.subtract(sum);
        senderAccBal.setAccountBalanceAmt(bigIntegerSen);
        accountBalanceRepo.save(senderAccBal);
    }

    private List<Account> setBalance(List<Account> accountList) {
        for (Account acc : accountList) {
            acc.setBalance(getAccountBalance(acc.getId()));
        }
        return accountList;
    }
}