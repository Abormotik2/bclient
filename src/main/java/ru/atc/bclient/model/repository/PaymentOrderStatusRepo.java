package ru.atc.bclient.model.repository;

import org.springframework.data.repository.CrudRepository;
import ru.atc.bclient.model.entity.PaymentOrderStatus;

public interface PaymentOrderStatusRepo extends CrudRepository<PaymentOrderStatus, Integer> {
    PaymentOrderStatus getPaymentOrderStatusByPaymentOrderStatusCode(String paymentOrderStatusCode);
}