package ru.atc.bclient.model.repository;

import org.springframework.data.repository.CrudRepository;
import ru.atc.bclient.model.entity.Operation;

public interface OperationRepo extends CrudRepository<Operation, Integer> {
}
