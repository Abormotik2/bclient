package ru.atc.bclient.model.repository;

import org.springframework.data.repository.CrudRepository;
import ru.atc.bclient.model.entity.Bank;

public interface BankRepo extends CrudRepository<Bank, Integer> {

}