package ru.atc.bclient.model.repository;

import org.springframework.data.repository.CrudRepository;
import ru.atc.bclient.model.entity.AccountStatus;

public interface AccountStatusRepo extends CrudRepository<AccountStatus, Integer> {
    AccountStatus getAccountStatusByAccountStatusCode(String accountStatusCode);
}