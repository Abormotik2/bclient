package ru.atc.bclient.model.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import ru.atc.bclient.model.entity.Account;
import ru.atc.bclient.model.entity.LegalEntity;

import java.util.List;

public interface AccountRepo extends CrudRepository<Account, Integer> {
    List<Account> getAccountByLegalEntity(LegalEntity legalEntity, Sort sort);

    List<Account> findAllByBankBankNameAndLegalEntity(String bankName, LegalEntity legalEntity, Sort sort);
}