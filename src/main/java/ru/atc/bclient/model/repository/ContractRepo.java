package ru.atc.bclient.model.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.atc.bclient.model.entity.Contract;
import ru.atc.bclient.model.entity.LegalEntity;

import java.util.Date;
import java.util.List;

public interface ContractRepo extends CrudRepository<Contract, Integer> {

    List<Contract> getContractsByIssuerOrSinger(LegalEntity issuer, LegalEntity singer, Sort sort);

    List<Contract> getContractsByIssuerAndSinger(LegalEntity issuer, LegalEntity singer, Sort sort);

    @Query("FROM Contract t " +
            "WHERE (t.issuer = ?1 OR t.singer = ?1) " +
            "  and (t.contractOpenDate >= ?2 or coalesce(?2, null) is null) " +
            "  and (t.contractOpenDate <= ?3 or coalesce(?3, null) is null) ")
    List<Contract> findAll(LegalEntity legalEntity, Date startDate, Date endDate, Sort sort);
}