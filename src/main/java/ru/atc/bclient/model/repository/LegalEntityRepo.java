package ru.atc.bclient.model.repository;

import org.springframework.data.repository.CrudRepository;
import ru.atc.bclient.model.entity.LegalEntity;

public interface LegalEntityRepo extends CrudRepository<LegalEntity, Integer> {
}
