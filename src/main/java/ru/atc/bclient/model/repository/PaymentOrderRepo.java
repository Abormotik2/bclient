package ru.atc.bclient.model.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.atc.bclient.model.entity.LegalEntity;
import ru.atc.bclient.model.entity.PaymentOrder;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

public interface PaymentOrderRepo extends CrudRepository<PaymentOrder, Integer> {
    @Query("select pay from PaymentOrder pay where pay.senderLegalEntity = ?1 or pay.recipientLegalEntity = ?1 order by pay.paymentOrderDate")
    List<PaymentOrder> getPaymentOrdersBySenderOrRecipient(LegalEntity legalEntity);

    @Query("select pay from PaymentOrder pay where pay.paymentOrderStatus in (1,4,6) order by pay.paymentOrderDate")
    List<PaymentOrder> getPaymentOrders();

    @Query("FROM PaymentOrder pay " +
            "WHERE (pay.senderLegalEntity = ?1 or pay.recipientLegalEntity = ?1) " +
            "  and (pay.paymentOrderDate >= ?2) " +
            "  and (pay.paymentOrderDate <= ?3) ")
    List<PaymentOrder> findAllByFilterDate(@NotNull LegalEntity legalEntity, @NotNull Date startDate, @NotNull Date endDate);
}