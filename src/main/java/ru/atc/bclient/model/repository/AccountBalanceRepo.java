package ru.atc.bclient.model.repository;

import org.springframework.data.repository.CrudRepository;
import ru.atc.bclient.model.entity.AccountBalance;

public interface AccountBalanceRepo extends CrudRepository<AccountBalance, Integer> {
    AccountBalance getTop1ByAccount_IdOrderByAccountBalanceDateDesc(int account_id);

    AccountBalance getTopByAccountId(int account_id);
}