package ru.atc.bclient.model.repository;

import org.springframework.data.repository.CrudRepository;
import ru.atc.bclient.model.entity.User;

import java.util.Optional;

public interface UserRepo extends CrudRepository<User, Integer> {
    Optional<User> findByUserLoginAndUserPassword(String userLogin, String userPassword);
}