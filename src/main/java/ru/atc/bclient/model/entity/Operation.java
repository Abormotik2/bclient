package ru.atc.bclient.model.entity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name = "fct_operation", schema = "bclient", catalog = "postgres")
public class Operation {
    private int id;
    private Date operationDate;
    private BigInteger operationAmt;
    private String operationDescr;
    private Account debetAccount;
    private Account creditAccount;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_operation_id")
    @SequenceGenerator(name="seq_operation_id", sequenceName="seq_operation_id", allocationSize=1)
    @Column(name = "operation_id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "operation_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }

    @Basic
    @Column(name = "operation_amt")
    public BigInteger getOperationAmt() {
        return operationAmt;
    }

    public void setOperationAmt(BigInteger operationAmt) {
        this.operationAmt = operationAmt;
    }

    @Basic
    @Column(name = "operation_descr")
    public String getOperationDescr() {
        return operationDescr;
    }

    public void setOperationDescr(String operationDescr) {
        this.operationDescr = operationDescr;
    }

    @ManyToOne
    @JoinColumn(name = "debet_account_id", referencedColumnName = "account_id", nullable = false)
    public Account getDebetAccount() {
        return debetAccount;
    }

    public void setDebetAccount(Account debetAccount) {
        this.debetAccount = debetAccount;
    }

    @ManyToOne
    @JoinColumn(name = "credit_account_id", referencedColumnName = "account_id", nullable = false)
    public Account getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(Account creditAccount) {
        this.creditAccount = creditAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Operation that = (Operation) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(operationDate, that.operationDate)
                .append(operationAmt, that.operationAmt)
                .append(operationDescr, that.operationDescr)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(operationDate)
                .append(operationAmt)
                .append(operationDescr)
                .toHashCode();
    }
}
