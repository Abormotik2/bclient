package ru.atc.bclient.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Timestamp;

@Entity
@Table(name = "fct_account_balance", schema = "bclient", catalog = "postgres")
public class AccountBalance {
    private int id;
    private Timestamp accountBalanceDate;
    private BigInteger accountBalanceAmt;
    private Account account;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_account_balance_id")
    @SequenceGenerator(name="seq_account_balance_id", sequenceName="seq_account_balance_id", allocationSize=1)
    @Column(name = "account_balance_id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "account_balance_date")
    public Timestamp getAccountBalanceDate() {
        return accountBalanceDate;
    }

    public void setAccountBalanceDate(Timestamp accountBalanceDate) {
        this.accountBalanceDate = accountBalanceDate;
    }

    @Basic
    @Column(name = "account_balance_amt")
    public BigInteger getAccountBalanceAmt() {
        return accountBalanceAmt;
    }

    public void setAccountBalanceAmt(BigInteger accountBalanceAmt) {
        this.accountBalanceAmt = accountBalanceAmt;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false)
    @JsonIgnore
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AccountBalance that = (AccountBalance) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(accountBalanceDate, that.accountBalanceDate)
                .append(accountBalanceAmt, that.accountBalanceAmt)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(accountBalanceDate)
                .append(accountBalanceAmt)
                .toHashCode();
    }
}