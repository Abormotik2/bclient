package ru.atc.bclient.model.entity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "dim_user", schema = "bclient", catalog = "postgres")
public class User {
    private int id;
    private String userLogin;
    private String userFullName;
    private String userPassword;
    private Set<LegalEntity> legalEntity;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_user_id")
    @SequenceGenerator(name="seq_user_id", sequenceName="seq_user_id", allocationSize=1)
    @Column(name = "user_id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_login")
    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Basic
    @Column(name = "user_full_name")
    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    @Basic
    @Column(name = "user_password")
    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "rel_user_legal_entity", joinColumns=@JoinColumn(name="user_id"),inverseJoinColumns=@JoinColumn(name="legal_entity_id"))
    public Set<LegalEntity> getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(Set<LegalEntity> legalEntity) {
        this.legalEntity = legalEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        User dimUser = (User) o;

        return new EqualsBuilder()
                .append(id, dimUser.id)
                .append(userLogin, dimUser.userLogin)
                .append(userFullName, dimUser.userFullName)
                .append(userPassword, dimUser.userPassword)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(userLogin)
                .append(userFullName)
                .append(userPassword)
                .toHashCode();
    }

    @Override
    public String toString() {
        return userFullName;
    }
}
