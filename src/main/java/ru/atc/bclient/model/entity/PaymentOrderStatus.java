package ru.atc.bclient.model.entity;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "dim_payment_order_status", schema = "bclient", catalog = "postgres")
public class PaymentOrderStatus {

    @Id
    @Column(name = "payment_order_status_id")
    private int id;
    @Basic
    @Column(name = "payment_order_status_code")
    private String paymentOrderStatusCode;
    @Basic
    @Column(name = "payment_order_status_name")
    private String paymentOrderStatusName;
}
