package ru.atc.bclient.model.entity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name = "dim_legal_entity", schema = "bclient", catalog = "postgres")
public class LegalEntity {
    private int id;
    private String legalEntityShortName;
    private String legalEntityFullName;
    private String legalEntityInn;
    private String legalEntityKpp;
    private String legalEntityOgrn;
    private String legalAddress;
    private Set<User> users;
    private Set<Account> accounts;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_legal_entity_id")
    @SequenceGenerator(name="seq_legal_entity_id", sequenceName="seq_legal_entity_id", allocationSize=1)
    @Column(name = "legal_entity_id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "legal_entity_short_name")
    public String getLegalEntityShortName() {
        return legalEntityShortName;
    }

    public void setLegalEntityShortName(String legalEntityShortName) {
        this.legalEntityShortName = legalEntityShortName;
    }

    @Basic
    @Column(name = "legal_entity_full_name")
    public String getLegalEntityFullName() {
        return legalEntityFullName;
    }

    public void setLegalEntityFullName(String legalEntityFullName) {
        this.legalEntityFullName = legalEntityFullName;
    }

    @Basic
    @Column(name = "legal_entity_inn")
    public String getLegalEntityInn() {
        return legalEntityInn;
    }

    public void setLegalEntityInn(String legalEntityInn) {
        this.legalEntityInn = legalEntityInn;
    }

    @Basic
    @Column(name = "legal_entity_kpp")
    public String getLegalEntityKpp() {
        return legalEntityKpp;
    }

    public void setLegalEntityKpp(String legalEntityKpp) {
        this.legalEntityKpp = legalEntityKpp;
    }

    @Basic
    @Column(name = "legal_entity_ogrn")
    public String getLegalEntityOgrn() {
        return legalEntityOgrn;
    }

    public void setLegalEntityOgrn(String legalEntityOgrn) {
        this.legalEntityOgrn = legalEntityOgrn;
    }

    @Basic
    @Column(name = "legal_address")
    public String getLegalAddress() {
        return legalAddress;
    }

    public void setLegalAddress(String legalAddress) {
        this.legalAddress = legalAddress;
    }

    @ManyToMany(mappedBy = "legalEntity")
    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @OneToMany(mappedBy = "legalEntity")
    public Set<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        LegalEntity that = (LegalEntity) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(legalEntityShortName, that.legalEntityShortName)
                .append(legalEntityFullName, that.legalEntityFullName)
                .append(legalEntityInn, that.legalEntityInn)
                .append(legalEntityKpp, that.legalEntityKpp)
                .append(legalEntityOgrn, that.legalEntityOgrn)
                .append(legalAddress, that.legalAddress)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(legalEntityShortName)
                .append(legalEntityFullName)
                .append(legalEntityInn)
                .append(legalEntityKpp)
                .append(legalEntityOgrn)
                .append(legalAddress)
                .toHashCode();
    }

    @Override
    public String toString() {
        return legalEntityFullName;
    }
}
