package ru.atc.bclient.model.entity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "dim_bank", schema = "bclient", catalog = "postgres")
public class Bank {
    private int id;
    private String bankName;
    private String bankInn;
    private String bankKpp;
    private String bankBic;
    private String bankCorrAcc;
    private Set<Account> accounts;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_bank_id")
    @SequenceGenerator(name="seq_bank_id", sequenceName="seq_bank_id", allocationSize=1)
    @Column(name = "bank_id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "bank_name")
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Basic
    @Column(name = "bank_inn")
    public String getBankInn() {
        return bankInn;
    }

    public void setBankInn(String bankInn) {
        this.bankInn = bankInn;
    }

    @Basic
    @Column(name = "bank_kpp")
    public String getBankKpp() {
        return bankKpp;
    }

    public void setBankKpp(String bankKpp) {
        this.bankKpp = bankKpp;
    }

    @Basic
    @Column(name = "bank_bic")
    public String getBankBic() {
        return bankBic;
    }

    public void setBankBic(String bankBic) {
        this.bankBic = bankBic;
    }

    @Basic
    @Column(name = "bank_corr_acc")
    public String getBankCorrAcc() {
        return bankCorrAcc;
    }

    public void setBankCorrAcc(String bankCorrAcc) {
        this.bankCorrAcc = bankCorrAcc;
    }

    @OneToMany(mappedBy = "bank")
    public Set<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Bank dimBank = (Bank) o;

        return new EqualsBuilder()
                .append(id, dimBank.id)
                .append(bankName, dimBank.bankName)
                .append(bankInn, dimBank.bankInn)
                .append(bankKpp, dimBank.bankKpp)
                .append(bankBic, dimBank.bankBic)
                .append(bankCorrAcc, dimBank.bankCorrAcc)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(bankName)
                .append(bankInn)
                .append(bankKpp)
                .append(bankBic)
                .append(bankCorrAcc)
                .toHashCode();
    }

    @Override
    public String toString() {
        return bankName;
    }
}