package ru.atc.bclient.model.entity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "dim_contract", schema = "bclient", catalog = "postgres")
public class Contract {
    private int id;
    private String contractName;
    private String contractNum;
    private Date contractOpenDate;
    private Date contractCloseDate;
    private String currencyCode;
    private LegalEntity issuer;
    private LegalEntity singer;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_contract_id")
    @SequenceGenerator(name="seq_contract_id", sequenceName="seq_contract_id", allocationSize=1)
    @Column(name = "contract_id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "contract_name")
    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    @Basic
    @Column(name = "contract_num")
    public String getContractNum() {
        return contractNum;
    }

    public void setContractNum(String contractNum) {
        this.contractNum = contractNum;
    }

    @Basic
    @Column(name = "contract_open_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    public Date getContractOpenDate() {
        return contractOpenDate;
    }

    public void setContractOpenDate(Date contractOpenDate) {
        this.contractOpenDate = contractOpenDate;
    }

    @Basic
    @Column(name = "contract_close_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    public Date getContractCloseDate() {
        return contractCloseDate;
    }

    public void setContractCloseDate(Date contractCloseDate) {
        this.contractCloseDate = contractCloseDate;
    }

    @Basic
    @Column(name = "currency_code")
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @ManyToOne
    @JoinColumn(name = "issuer_legal_entity_id", referencedColumnName = "legal_entity_id", nullable = false)
    public LegalEntity getIssuer() {
        return issuer;
    }

    public void setIssuer(LegalEntity issuer) {
        this.issuer = issuer;
    }

    @ManyToOne
    @JoinColumn(name = "signer_legal_entity_id", referencedColumnName = "legal_entity_id", nullable = false)
    public LegalEntity getSinger() {
        return singer;
    }

    public void setSinger(LegalEntity singer) {
        this.singer = singer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Contract that = (Contract) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(contractName, that.contractName)
                .append(contractNum, that.contractNum)
                .append(contractOpenDate, that.contractOpenDate)
                .append(contractCloseDate, that.contractCloseDate)
                .append(currencyCode, that.currencyCode)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(contractName)
                .append(contractNum)
                .append(contractOpenDate)
                .append(contractCloseDate)
                .append(currencyCode)
                .toHashCode();
    }

    @Override
    public String toString() {
        return contractName;
    }
}