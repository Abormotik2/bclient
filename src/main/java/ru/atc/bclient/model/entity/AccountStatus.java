package ru.atc.bclient.model.entity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;

@Entity
@Table(name = "dim_account_status", schema = "bclient", catalog = "postgres")
public class AccountStatus {
    private int id;
    private String accountStatusCode;
    private String accountStatusName;

    @Id
    @Column(name = "account_status_id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "account_status_code")
    public String getAccountStatusCode() {
        return accountStatusCode;
    }

    public void setAccountStatusCode(String accountStatusCode) {
        this.accountStatusCode = accountStatusCode;
    }

    @Basic
    @Column(name = "account_status_name")
    public String getAccountStatusName() {
        return accountStatusName;
    }

    public void setAccountStatusName(String accountStatusName) {
        this.accountStatusName = accountStatusName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AccountStatus that = (AccountStatus) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(accountStatusCode, that.accountStatusCode)
                .append(accountStatusName, that.accountStatusName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(accountStatusCode)
                .append(accountStatusName)
                .toHashCode();
    }

    @Override
    public String toString() {
        return accountStatusName;
    }
}