package ru.atc.bclient.model.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "dim_account", schema = "bclient", catalog = "postgres")
public class Account {
    private int id;
    private String accountName;
    private String accountNum;
    private String currencyCode;
    private AccountStatus accountStatus;
    private Bank bank;
    private LegalEntity legalEntity;
    private AccountBalance balance;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_account_id")
    @SequenceGenerator(name = "seq_account_id", sequenceName = "seq_account_id", allocationSize = 1)
    @Column(name = "account_id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "account_name")
    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @Basic
    @Column(name = "account_num")
    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    @Basic
    @Column(name = "currency_code")
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @OneToOne
    @JoinColumn(name = "account_status_id", referencedColumnName = "account_status_id", nullable = false)
    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    @ManyToOne
    @JoinColumn(name = "bank_id", referencedColumnName = "bank_id", nullable = false)
    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    @ManyToOne
    @JoinColumn(name = "legal_entity_id", referencedColumnName = "legal_entity_id")
    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    @Transient
    public AccountBalance getBalance() {
        return balance;
    }

    public void setBalance(AccountBalance balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                Objects.equals(accountName, account.accountName) &&
                Objects.equals(accountNum, account.accountNum) &&
                Objects.equals(currencyCode, account.currencyCode) &&
                Objects.equals(accountStatus, account.accountStatus) &&
                Objects.equals(bank, account.bank) &&
                Objects.equals(legalEntity, account.legalEntity) &&
                Objects.equals(balance, account.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountName, accountNum, currencyCode, accountStatus, bank, legalEntity, balance);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", accountName='" + accountName + '\'' +
                ", accountNum='" + accountNum + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", accountStatus=" + accountStatus +
                ", bank=" + bank +
                ", legalEntity=" + legalEntity +
                ", balance=" + balance +
                '}';
    }
}