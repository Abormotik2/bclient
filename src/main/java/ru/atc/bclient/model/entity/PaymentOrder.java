package ru.atc.bclient.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name = "fct_payment_order", schema = "bclient", catalog = "postgres")
public class PaymentOrder {
    private int id;
    private int paymentOrderNum;
    private Date paymentOrderDate;
    private String currencyCode;
    private BigInteger paymentOrderAmt;
    private String paymentReason;
    private String paymentPriorityCode;
    private String rejectReason;
    private Contract contract;
    private PaymentOrderStatus paymentOrderStatus;
    private Account recipientAccount;
    private LegalEntity recipientLegalEntity;
    private Account senderAccount;
    private LegalEntity senderLegalEntity;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_payment_order_id")
    @SequenceGenerator(name="seq_payment_order_id", sequenceName="seq_payment_order_id", allocationSize=1)
    @Column(name = "payment_order_id")
    @JsonIgnore
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "payment_order_num")
    public int getPaymentOrderNum() {
        return paymentOrderNum;
    }

    public void setPaymentOrderNum(int paymentOrderNum) {
        this.paymentOrderNum = paymentOrderNum;
    }

    @Basic
    @Column(name = "payment_order_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    public Date getPaymentOrderDate() {
        return paymentOrderDate;
    }

    public void setPaymentOrderDate(Date paymentOrderDate) {
        this.paymentOrderDate = paymentOrderDate;
    }

    @Basic
    @Column(name = "currency_code")
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Basic
    @Column(name = "payment_order_amt")
    @NotNull(message = "Введите сумму")
    @Digits(message = "Номер счета должен состоять только из цифр", integer = 10, fraction = 2)
    public BigInteger getPaymentOrderAmt() {
        return paymentOrderAmt;
    }

    public void setPaymentOrderAmt(BigInteger paymentOrderAmt) {
        this.paymentOrderAmt = paymentOrderAmt;
    }

    @Basic
    @Column(name = "payment_reason")
    public String getPaymentReason() {
        return paymentReason;
    }

    public void setPaymentReason(String paymentReason) {
        this.paymentReason = paymentReason;
    }

    @Basic
    @Column(name = "payment_priority_code")
    public String getPaymentPriorityCode() {
        return paymentPriorityCode;
    }

    public void setPaymentPriorityCode(String paymentPriorityCode) {
        this.paymentPriorityCode = paymentPriorityCode;
    }

    @Basic
    @Column(name = "reject_reason")
    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    @JsonIgnore
    @JsonProperty("contract")
    @ManyToOne
    @JoinColumn(name = "contract_id", referencedColumnName = "contract_id")
    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    @ManyToOne
    @JoinColumn(name = "payment_order_status_id", referencedColumnName = "payment_order_status_id", nullable = false)
    public PaymentOrderStatus getPaymentOrderStatus() {
        return paymentOrderStatus;
    }

    public void setPaymentOrderStatus(PaymentOrderStatus paymentOrderStatus) {
        this.paymentOrderStatus = paymentOrderStatus;
    }

    @JsonIgnore
    @JsonProperty("recipientAccount")
    @ManyToOne
    @JoinColumn(name = "recipient_account_id", referencedColumnName = "account_id", nullable = false)
    public Account getRecipientAccount() {
        return recipientAccount;
    }

    public void setRecipientAccount(Account recipientAccount) {
        this.recipientAccount = recipientAccount;
    }

    @JsonIgnore
    @JsonProperty("recipientLegalEntity")
    @ManyToOne
    @JoinColumn(name = "recipient_legal_entity_id", referencedColumnName = "legal_entity_id", nullable = false)
    public LegalEntity getRecipientLegalEntity() {
        return recipientLegalEntity;
    }

    public void setRecipientLegalEntity(LegalEntity recipientLegalEntity) {
        this.recipientLegalEntity = recipientLegalEntity;
    }

    @JsonIgnore
    @JsonProperty("senderAccount")
    @ManyToOne
    @JoinColumn(name = "sender_account_id", referencedColumnName = "account_id", nullable = false)
    public Account getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(Account senderAccount) {
        this.senderAccount = senderAccount;
    }

    @JsonIgnore
    @JsonProperty("senderLegalEntity")
    @ManyToOne
    @JoinColumn(name = "sender_legal_entity_id", referencedColumnName = "legal_entity_id", nullable = false)
    public LegalEntity getSenderLegalEntity() {
        return senderLegalEntity;
    }

    public void setSenderLegalEntity(LegalEntity senderLegalEntity) {
        this.senderLegalEntity = senderLegalEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PaymentOrder that = (PaymentOrder) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(paymentOrderNum, that.paymentOrderNum)
                .append(paymentOrderDate, that.paymentOrderDate)
                .append(currencyCode, that.currencyCode)
                .append(paymentOrderAmt, that.paymentOrderAmt)
                .append(paymentReason, that.paymentReason)
                .append(paymentPriorityCode, that.paymentPriorityCode)
                .append(rejectReason, that.rejectReason)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(paymentOrderNum)
                .append(paymentOrderDate)
                .append(currencyCode)
                .append(paymentOrderAmt)
                .append(paymentReason)
                .append(paymentPriorityCode)
                .append(rejectReason)
                .toHashCode();
    }
}
