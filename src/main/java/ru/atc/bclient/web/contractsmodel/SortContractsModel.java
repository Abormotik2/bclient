package ru.atc.bclient.web.contractsmodel;

import java.util.Arrays;
import java.util.List;

public class SortContractsModel {

    private final List<SortContractsType> settingSort = Arrays.asList(SortContractsType.values());
    private SortContractsType chooseContType = SortContractsType.CONTRACT_NAME;

    public List<SortContractsType> getSettingSort() {
        return settingSort;
    }

    public SortContractsType getChooseContType() {
        return chooseContType;
    }

    public void setChooseContType(SortContractsType chooseContType) {
        this.chooseContType = chooseContType;
    }
}

