package ru.atc.bclient.web.contractsmodel;

public enum SortContractsType {
    CONTRACT_NAME("Наименование договора", "contractName"),
    CONTRACT_NUM("Номер договора", "contractNum"),
    DATE("Дата заключения договора", "contractOpenDate"),
    ISSUER("Эмитент договора", "issuer"),
    CONTRACT_SINGER("Подписант договора", "singer");

    private final String value;
    private final String sortProperty;

    SortContractsType(String value, String sortProperty) {
        this.value = value;
        this.sortProperty = sortProperty;
    }

    public String getValue() {
        return value;
    }

    public String getSort() {
        return sortProperty;
    }
}