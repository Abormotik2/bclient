package ru.atc.bclient.web.validator;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.atc.bclient.model.entity.Account;

@Component
public class AccountValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Account.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Account account = (Account) o;
        if(account.getAccountName() == null || account.getAccountName().length() == 0){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "accountName", "", "Введите название счета");
        } else if (account.getAccountName().length() < 3){
            errors.rejectValue("accountName", "", "Навание должно содержать не менее 3 символов");
        }

        if(account.getAccountNum() == null || account.getAccountNum().length() == 0){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "accountNum", "", "Введите номер счета");
        } else if(account.getAccountNum().length() != 20){
            errors.rejectValue("accountNum", "", "Номер счета должен состоять из 20 цифр");
        } else if (!NumberUtils.isNumber(account.getAccountNum())){
            errors.rejectValue("accountNum", "", "Номер счета должен состоять только из цифр");
        }

        if(account.getCurrencyCode() == null || account.getCurrencyCode().length() == 0){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "currencyCode", "", "Введите код валюты");
        }
    }
}
