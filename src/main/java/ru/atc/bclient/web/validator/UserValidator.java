package ru.atc.bclient.web.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.atc.bclient.model.entity.User;

@Component
public class UserValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;
        if(user.getUserLogin() == null || user.getUserLogin().length() == 0){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userLogin", "", "Введите имя пользователя");
        } else if (user.getUserLogin().length() <= 3){
            errors.rejectValue("userLogin", "", "Имя должно быть больше 3 символов");
        }

        if(user.getUserPassword() == null || user.getUserPassword().length() == 0){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userPassword", "", "Введите пароль");
        } else if(user.getUserPassword().length() <= 5){
            errors.rejectValue("userPassword", "", "Пароль должен быть больше 5 символов");
        }
    }
}
