package ru.atc.bclient.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.atc.bclient.model.entity.AccountBalance;
import ru.atc.bclient.model.entity.PaymentOrder;
import ru.atc.bclient.service.AccountsService;
import ru.atc.bclient.service.LegalEntityService;
import ru.atc.bclient.service.PaymentOrderService;
import ru.atc.bclient.web.datamodel.DateFilterModel;

import java.util.List;

@RestController
@RequestMapping(path = "/rest")
public class ApiController {

    @Autowired
    private AccountsService accountsService;

    @Autowired
    private PaymentOrderService paymentOrderService;

    @Autowired
    private LegalEntityService legalEntityService;

    @GetMapping(path = "/account/balance")
    public AccountBalance getAccountBalance(@RequestParam("id") int accountId) {
        return accountsService.getAccountBalance(accountId);
    }

    @GetMapping(path = "/account/paymentList/{id}")
    public List<PaymentOrder> getPaymentOrders(@PathVariable("id") int legalId, DateFilterModel dateFilterModel) {
        return paymentOrderService.getPaymentOrderByFilterDate(legalEntityService.getLegalEntityById(legalId), dateFilterModel);
    }

    @PostMapping(path = "/paymentOrder/createPaymentOrder")
    public PaymentOrder creatPaymentOrder(@RequestBody PaymentOrder paymentOrder) {
        return paymentOrderService.createPaymentOrder(paymentOrder);
    }

    @PostMapping(path = "/paymentOrder/deletePaymentOrder/")
        public void deletePaymentOrder(@RequestParam("id") int paymentId){
        paymentOrderService.deletePaymentOrder(paymentId);
    }

    //test
    @GetMapping(path = "/hello")
    public String hello() {
        return "hello";
    }
}