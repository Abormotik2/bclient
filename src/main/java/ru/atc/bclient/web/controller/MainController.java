package ru.atc.bclient.web.controller;

import com.ibm.icu.text.RuleBasedNumberFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.atc.bclient.model.entity.*;
import ru.atc.bclient.service.*;
import ru.atc.bclient.web.contractsmodel.SortContractsModel;
import ru.atc.bclient.web.datamodel.DateFilterModel;
import ru.atc.bclient.web.paymentmodel.SortPaymentModel;
import ru.atc.bclient.web.validator.AccountValidator;
import ru.atc.bclient.web.validator.ContractValidator;
import ru.atc.bclient.web.validator.UserValidator;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
@SessionAttributes({"user", "legalEntity", "account", "contract", "paymentOrder", "sortAcc", "sortCont", "bankFilter", "dateFilterContracts", "dateFilterPayment"})
public class MainController {

    @Autowired
    LoginService loginService;
    @Autowired
    AccountsService accountsService;
    @Autowired
    LegalEntityService legalEntityService;
    @Autowired
    ContractsService contractsService;
    @Autowired
    PaymentOrderService paymentOrderService;
    @Autowired
    UserValidator userValidator;
    @Autowired
    AccountValidator accountValidator;
    @Autowired
    ContractValidator contractValidator;

    @GetMapping(value = "/")
    public String login(Model model) {
        model.addAttribute("user", new User());
        return "login";
    }

    @PostMapping(value = "/login")
    public ModelAndView executeLogin(@Valid @ModelAttribute("user") User user, BindingResult bindingResult, ModelAndView model) {
        if (bindingResult.hasErrors()) {
            model.setViewName("login");
            return model;
        }
        Optional<User> opUser = loginService.getUserByLoginAndPassword(user.getUserLogin(), user.getUserPassword());
        if (opUser.isPresent()) {
            model.addObject("user", opUser.get());
            model.setViewName("redirect:/welcome");
        } else {
            model.addObject("user", user);
            model.addObject("message", "Неверный логин или пароль");
            model.setViewName("login");
        }
        return model;
    }

    @GetMapping(value = "/welcome")
    public String welcomePage(@ModelAttribute("user") User user, Model model) {
        String name = user.getUserFullName().substring(user.getUserFullName().indexOf(" ") + 1);
        model.addAttribute("name", name);
        model.addAttribute("legalEntity", new LegalEntity());
        userLegalEntityList(model, user);
        return "welcome";
    }

    @PostMapping(value = "/welcome", params = "param")
    public String welcomePostPage(@ModelAttribute("legalEntity") LegalEntity legalEntity,
                                  @RequestParam(name = "param") String param, Model model) {
        legalEntity = legalEntityService.getLegalEntityById(legalEntity.getId());
        model.addAttribute("legalEntity", legalEntity);
        switch (param) {
            case "Счета":
                model.addAttribute("account", new Account());
                model.addAttribute("sortAcc", new SortPaymentModel());
                model.addAttribute("bankFilter", new Bank());
                return "redirect:/accounts";
            case "Договора":
                model.addAttribute("contract", new Contract());
                model.addAttribute("sortCont", new SortContractsModel());
                model.addAttribute("dateFilterContracts", new DateFilterModel());
                return "redirect:/contracts";
            case "Платежи":
                model.addAttribute("dateFilterPayment", new DateFilterModel());
                return "redirect:/payorderlist";
            case "Выход":
                return "redirect:/";
        }
        return "welcome";
    }

    @GetMapping(value = "/accounts")
    public String accountsPage(@ModelAttribute("legalEntity") LegalEntity legalEntity
            , @ModelAttribute("account") Account account
            , @ModelAttribute("sortAcc") SortPaymentModel sortPaymentModel
            , @ModelAttribute("bankFilter") Bank bank
            , Model model) {

        bankList(model);
        accountList(model, legalEntity, sortPaymentModel, bank);
        model.addAttribute("account", account);
        model.addAttribute("sortAcc", sortPaymentModel);
        return "accounts";
    }

    @PostMapping(value = "/accounts")
    public String addAccount(@Valid @ModelAttribute("account") Account account, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "accounts";
        }
        accountsService.createNewAccount(account);
        return "redirect:/accounts";
    }

    @GetMapping(value = "/contracts")
    public String contractsPage(@ModelAttribute("legalEntity") LegalEntity legalEntity,
                                @ModelAttribute("contract") Contract contract,
                                @ModelAttribute("sortCont") SortContractsModel sortContractsModel,
                                @ModelAttribute("dateFilterContracts") DateFilterModel dateFilterContracts,
                                Model model) {
        contractsList(model, legalEntity, sortContractsModel, dateFilterContracts);
        legalEntityAllList(model, legalEntity);
        getRandomNum(model);
        model.addAttribute("contract", contract);
        model.addAttribute("paymentOrder", new PaymentOrder());
        model.addAttribute("sortCont", sortContractsModel);
        model.addAttribute("dateFilterContracts", dateFilterContracts);
        return "contracts";
    }

    @PostMapping(value = "/contracts")
    public String contractsPostPage(@ModelAttribute("legalEntity") LegalEntity legalEntity,
                                    @Valid @ModelAttribute("contract") Contract contract,
                                    @ModelAttribute("sortCont") SortContractsModel sortContractsModel,
                                    @ModelAttribute("dateFilterContracts") DateFilterModel dateFilterContracts,
                                    BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "contracts";
        }
        contract = contractsService.createContract(contract, legalEntity);
        contractsList(model, legalEntity, sortContractsModel, dateFilterContracts);
        legalEntityAllList(model, legalEntity);
        getRandomNum(model);
        model.addAttribute("contract", contract);
        model.addAttribute("sortCont", sortContractsModel);
        model.addAttribute("dateFilterContracts", dateFilterContracts);
        return "contracts";
    }

    @GetMapping(value = "/payorderlist")
    public String payOrderListPage(@ModelAttribute("legalEntity") LegalEntity legalEntity,
                                   @ModelAttribute("dateFilterPayment") DateFilterModel dateFilterModel,
                                   @RequestParam(name = "param", required = false) String param, Model model) {
        if (param != null && param.equals("Обновить статус")) {
            paymentOrderService.processing();
        }
        paymentOrders(model, legalEntity, dateFilterModel);
        return "payorderlist";
    }

    @PostMapping(value = "/payorder")
    public String payOrderGetPage(@ModelAttribute("paymentOrder") PaymentOrder paymentOrder, SortPaymentModel sortPaymentModel, Model model, Bank bank) {
        Contract contract = contractsService.getContractById(paymentOrder.getContract().getId());
        singerAccounts(model, contract.getSinger());
        accountList(model, contract.getIssuer(), sortPaymentModel, bank);
        contractsList(model, contract.getIssuer(), new SortContractsModel(), new DateFilterModel());
        currentDate(model);
        getRandomNum(model);
        model.addAttribute("contract", contract);
        return "payorder";
    }

    @PostMapping(value = "/completepayorder")
    public String completePayOrder(@ModelAttribute("contract") Contract contract,
                                   @ModelAttribute("dateFilterPayment") DateFilterModel dateFilterModel,
                                   @Valid @ModelAttribute("paymentOrder") PaymentOrder paymentOrder, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            singerAccounts(model, contract.getSinger());
            return "payorder";
        }
        paymentOrder = paymentOrderService.fillPaymentOrder(paymentOrder, contract);
        model.addAttribute("amtText", getNumberText(paymentOrder));
        model.addAttribute("orderStrDate", new SimpleDateFormat("dd.MM.yyyy").format(paymentOrder.getPaymentOrderDate()));
        model.addAttribute("contractStrDate", new SimpleDateFormat("dd.MM.yyyy").format(contract.getContractOpenDate()));
        model.addAttribute("paymentOrder", paymentOrder);
        model.addAttribute("dateFilterPayment", dateFilterModel);
        return "completepayorder";
    }

    @GetMapping(value = "/paid")
    public String paidSuccessfully(@ModelAttribute("paymentOrder") PaymentOrder paymentOrder, Model model) {
        boolean create = paymentOrderService.createPayment(paymentOrder);
        if (create) {
            model.addAttribute("message", "Платежное поручение успешно отправлено");
        } else {
            model.addAttribute("message", "Сервис временно не доступен, попробуйте позже");
        }
        return "paid";
    }

    @ModelAttribute
    public void bankList(Model model) {
        List<Bank> banks = accountsService.getBanks();
        model.addAttribute("bankList", banks);
    }

    @ModelAttribute
    public void userLegalEntityList(Model model, User user) {
        Set<LegalEntity> legalEntities = user.getLegalEntity();
        model.addAttribute("legalSet", legalEntities);
    }

    @ModelAttribute
    public void legalEntityAllList(Model model, LegalEntity legalEntity) {
        List<LegalEntity> legalEntities = legalEntityService.getAllLegalEntity();
        legalEntities.remove(legalEntity);
        model.addAttribute("legalEntityAllList", legalEntities);
    }

    @ModelAttribute
    public void accountList(Model model, LegalEntity legalEntity, SortPaymentModel sortPaymentModel, Bank bank) {
        LegalEntity legalEntityById = getLegalEntityByID(legalEntity);
        String sort = sortPaymentModel.getChooseType().getSort();
        List<Account> accounts = bank.getBankName() == null
                ? accountsService.getAccounts(legalEntityById, sort)
                : accountsService.getAccountsByBank(bank.getBankName(), legalEntityById, sort);
        model.addAttribute("availableAccounts", accounts);
    }

    @ModelAttribute
    public void contractsList(Model model, LegalEntity legalEntity, SortContractsModel sortContractsModel, DateFilterModel date) {
        LegalEntity legalEntityById = getLegalEntityByID(legalEntity);
        String sort = sortContractsModel.getChooseContType().getSort();
        model.addAttribute("contractsList", contractsService.getContractsByFilterDate(legalEntityById, date, sort));
    }

    @ModelAttribute
    public void singerAccounts(Model model, LegalEntity singer) {
        List<Account> singerAccounts = accountsService.getAccounts(legalEntityService.getLegalEntityById(singer.getId()), (String) null);
        model.addAttribute("singerAccounts", singerAccounts);
    }

    @ModelAttribute
    public void paymentOrders(Model model, LegalEntity legalEntity, DateFilterModel date) {
//        List<PaymentOrder> paymentOrders = paymentOrderService.getPaymentOrders(legalEntityService.getLegalEntityById(legalEntity.getId()));
        List<PaymentOrder> paymentOrders = paymentOrderService.getPaymentOrderByFilterDate(legalEntityService.getLegalEntityById(legalEntity.getId()), date);
        model.addAttribute("paymentOrders", paymentOrders);
    }

    @ModelAttribute
    public void currentDate(Model model) {
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String curDate = currentDate.format(timeFormatter);
        model.addAttribute("currentDate", curDate);
    }

    @ModelAttribute
    public void getRandomNum(Model model) {
        final Random random = new Random();
        String num = String.valueOf(random.nextInt(100000) + 1);
        model.addAttribute("randomNum", num);
    }

    @InitBinder("contract")
    public void dataBinding(WebDataBinder binder) {
        binder.addValidators(contractValidator);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, "contract_open_date", new CustomDateEditor(dateFormat, true));
        binder.registerCustomEditor(Date.class, "contract_close_date", new CustomDateEditor(dateFormat, true));
    }

    @InitBinder("user")
    public void userBinding(WebDataBinder binder) {
        binder.addValidators(userValidator);
    }

    @InitBinder("account")
    public void accountBinding(WebDataBinder binder) {
        binder.addValidators(accountValidator);
    }

    private String getNumberText(PaymentOrder paymentOrder) {
        RuleBasedNumberFormat nf = new RuleBasedNumberFormat(Locale.forLanguageTag("ru"), RuleBasedNumberFormat.SPELLOUT);
        return nf.format(paymentOrder.getPaymentOrderAmt()) + " руб.";
    }

    private LegalEntity getLegalEntityByID(LegalEntity legalEntity) {
        return legalEntityService.getLegalEntityById(legalEntity.getId());
    }
}