package ru.atc.bclient.web.paymentmodel;

public enum SortPaymentType {
    STATUS("Статус", "accountStatus"),
    NAME_ACCOUNT("Наименование", "accountName"),
    NUMBER_ACC("Номер счета", "accountNum"),
    BANK("Банк", "bank.bankName"),
    BALANCE_ACC("Баланс","currencyCode"),
    CURRENCY("Валюта", "currencyCode");

    private final String value;
    private final String sortProperty;

    SortPaymentType(String value, String sortProperty) {
        this.value = value;
        this.sortProperty = sortProperty;
    }

    public String getValue() {
        return value;
    }

    public String getSort() {
        return sortProperty;
    }
}