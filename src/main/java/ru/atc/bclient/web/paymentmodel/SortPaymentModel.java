package ru.atc.bclient.web.paymentmodel;

import java.util.Arrays;
import java.util.List;

public class SortPaymentModel {
    private final List<SortPaymentType> settingSort = Arrays.asList(SortPaymentType.values());
    private SortPaymentType chooseAccType = SortPaymentType.STATUS;

    public List<SortPaymentType> getSettingSort() {
        return settingSort;
    }

    public SortPaymentType getChooseType() {
        return chooseAccType;
    }

    public void setChooseType(SortPaymentType chooseType) {
        this.chooseAccType = chooseType;
    }
}
