<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Payment Orders List</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="<c:url value="/resources/css/main.css"/>" rel="stylesheet" type="text/css">
</head>
<body>
<div class="main_header container-fluid">
    <p>${legalEntity.legalEntityFullName}</p>
    <form:form method="get">
        <input class="btn btn-light" type="button" value="Назад" onclick="window.location.href = '${pageContext.request.contextPath}/welcome'">
        <input class="btn btn-light" type="submit" name="param" value="Обновить статус" onclick="location.href = '/payorderlist';">
    </form:form>
</div>
<div class="dataFilter container-fluid" align="left">
    <span>Фильтр даты:</span>
    <div class="sort_select">
        <form:form id="dateFilterPayment" action="payorderlist" method="get" modelAttribute="dateFilterPayment">
            <form:input class="custom-select col-xs-2" path="startDate" type="date"/>
            <form:input class="custom-select col-xs-2"  path="endDate" type="date"/>
            <input class="btn btn-light" type="submit"/>
        </form:form>
    </div>
</div>
<div class="table">
    <table class="table-striped">
        <thead class="table-dark">
        <tr>
            <td>Номер</td>
            <td>Дата</td>
            <td>Отправитель</td>
            <td>Получатель</td>
            <td>Договор</td>
            <td>Сумма</td>
            <td>Статус платежа</td>
        </tr>
        </thead>
        <c:forEach items="${paymentOrders}" var="payOrd">
            <tr>
                <td>${payOrd.paymentOrderNum}</td>
                <td>${payOrd.paymentOrderDate}</td>
                <td>${payOrd.senderLegalEntity.legalEntityFullName}</td>
                <td>${payOrd.recipientLegalEntity.legalEntityFullName}</td>
                <td>${payOrd.contract.contractName}</td>
                <td>${payOrd.paymentOrderAmt}</td>
                <td>${payOrd.paymentOrderStatus.paymentOrderStatusName}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
