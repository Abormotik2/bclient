<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Paid successfully</title>
    <link href="<c:url value="/resources/css/main.css"/>" rel="stylesheet" type="text/css">
</head>
<body>
<div class="main_header">
    <p>${message}</p>
    <input class="button" type="button" value="Вернуться в личный кабинет" onclick="location.href = '${pageContext.request.contextPath}/welcome'">
</div>
</body>
</html>
