<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Payment Order</title>
    <link href="<c:url value="/resources/css/main.css"/>" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="main_header">
        <p>Введите данные</p>
    </div>
    <div class="pay_order_panel" id="create_payment_order_form">
        <form:form id="order" action="completepayorder" method="post" modelAttribute="paymentOrder">
            <table>
                <tr>
                    <td colspan="4">Наименование договора:
                        <label>${contract.contractName}</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><label>Платежное поручение № </label>
                        <form:input class="create_input" id="pay_order_num" name="pay_order_num" value="${randomNum}" path="paymentOrderNum" placeholder="Номер"/>
                    </td>
                    <td colspan="2"><label>Дата: </label>
                        <form:label id="currentDate" name="currentDate" path="paymentOrderDate">${currentDate}</form:label>
                    </td>
                </tr>
                <tr>
                    <td><label>Получатель: </label></td>
                    <td><form:label id="recipientLegalEntity" name="recipientLegalEntity" path="recipientLegalEntity">${contract.issuer}</form:label></td>
                    <td><label>Счет получателя: </label></td>
                    <td>
                        <form:select class="create_select" path="recipientAccount.id">
                            <form:options items="${availableAccounts}" itemValue="id" itemLabel="accountName"/>
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td><label>Плательщик: </label></td>
                    <td><form:label id="senderLegalEntity" name="senderLegalEntity" path="senderLegalEntity">${contract.singer}</form:label></td>
                    <td><label>Счет плательщика: </label></td>
                    <td><form:select class="create_select" path="senderAccount.id">
                            <form:options items="${singerAccounts}" itemValue="id" itemLabel="accountName"/>
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td colspan="4"><label>Сумма: </label>
                        <form:input class="create_input" id="pay_order_amount" name="pay_order_amount" path="paymentOrderAmt" placeholder="Сумма" />
                        <label class="error"><form:errors path="*"/></label>
                    </td>
                </tr>
            </table>
            <input class="button_cnsl" type="button" value="Отмена" onclick="location.href = '${pageContext.request.contextPath}/welcome'">
            <input class="button" type="submit" value="Сформировать платежное поручение">
        </form:form>
    </div>
</body>
</html>
