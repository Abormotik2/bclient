<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Login</title>
    <link href="<c:url value="/resources/css/main.css"/>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="col-xs-2 container-fluid alert-dark 2">
        <form:form id="loginForm" method="post" action="login" modelAttribute="user">
            <table>
                <tr>
                    <td><form:label path="userLogin">Login</form:label></td>
                    <td><form:input class="form-control mb-2" id="username" name="username" path="userLogin" /></td>
                </tr>
                <tr>
                    <td><form:label path="userPassword">Password</form:label></td>
                    <td><form:password class="form-control mb-2" id="password" name="password" path="userPassword" /></td>
                </tr>
            </table>
            <input class="btn btn-light" type="submit" value="Войти"/><br>
            <div class="error">
                <form:errors path="*"/>
                    ${message}
            </div>
        </form:form>

    </div>
</body>
</html>
