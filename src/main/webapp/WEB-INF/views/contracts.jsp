<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<head>
    <title>Contracts</title>
    <link href="<c:url value="/resources/css/main.css"/>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#contract_open_date").datepicker({
                dateFormat: 'dd.mm.yy'
            }).val();
            $("#contract_close_date").datepicker({
                dateFormat: 'dd.mm.yy'
            }).val();
        });
    </script>
</head>
<body>
    <form:form id="contract" action="contracts" method="post" modelAttribute="contract">
    <div class="main_header container-fluid">
        <div class="main_header">
            <p>${legalEntity.legalEntityFullName}</p>
            <input class="btn btn-light" type="button" value="Назад" onclick="window.location.href = '${pageContext.request.contextPath}/welcome'">
            <input class="btn btn-light" style="width:148px;height:38px;" type="button" value="Заключить договор"  onclick="document.getElementById('create_contract_form').style.display = 'block';">
            <div class="error"><form:errors path="*"/></div>
        </div>
        <div class="create_panel" id="create_contract_form">
            <form:input class="create_input" id="contract_name" name="contract_name" path="contractName" placeholder="Наименование договора" />
            <form:input class="create_input" id="contract_num" name="contract_num" value="${randomNum}" path="contractNum" placeholder="Номер договора" />
            <form:input class="create_input" id="contract_open_date" path="contractOpenDate" placeholder="Дата открытия"/>
            <form:input class="create_input" id="contract_close_date" path="contractCloseDate" placeholder="Дата закрытия"/>
            <form:select class="create_select" path="singer.id">
                <form:options items="${legalEntityAllList}" itemValue="id" itemLabel="legalEntityShortName"/>
            </form:select><br>
            <input class="button" type="button" onclick="document.getElementById('create_contract_form').style.display = 'none';" value="Отмена">
            <input class="button" type="submit" onclick="document.getElementById('create_contract_form').style.display = 'none';" value="Сохранить">
        </div>
    </form:form>



    <div class="pay_panel container-fluid" style="height: 40px;">
        <form:form id="payment_contract" action="payorder" modelAttribute="paymentOrder" method="post">
            <form:select class="custom-select col-xs-2" path="contract.id">
                <form:options items="${contractsList}" itemValue="id" itemLabel="contractName"/>
            </form:select>
            <input class="btn btn-light" type="submit" value="Оплата по договору">
        </form:form>
    </div>

    <div class="sort container-fluid" align="left">
        <span>Сортировка:</span>
        <div class="sort_select">
            <form:form id="sortCont" action="contracts" method="get" modelAttribute="sortCont">
                <form:select class="custom-select col-xs-2" path="chooseContType">
                    <form:options items="${sortCont.settingSort}" itemLabel="value"/>
                </form:select>
                <input class="btn btn-light" type="submit"/>
            </form:form>
        </div>
    </div>
    <div class="dataFilter container-fluid" align="left">
        <span>Фильтр даты открытия:</span>
        <div class="sort_select">
            <form:form id="dateFilter" action="contracts" method="get" modelAttribute="dateFilterContracts">
                <form:input class="custom-select col-xs-2" path="startDate" type="date"/>
                <form:input class="custom-select col-xs-2" path="endDate" type="date"/>
                <input class="btn btn-light" type="submit"/>
            </form:form>
        </div>
    </div>
<div class="table">
    <table class="table-striped">
        <thead class="table-dark">
        <tr>
            <td>Наименование договора</td>
            <td>Номер договора</td>
            <td>Дата заключения договора</td>
            <td>Эмитент договора</td>
            <td>Подписант договора</td>
        </tr>
        </thead>
        <c:forEach items="${contractsList}" var="con">
            <tr>
                <td>${con.contractName}</td>
                <td>${con.contractNum}</td>
                <td>${con.contractOpenDate}</td>
                <td>${con.issuer}</td>
                <td>${con.singer}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
