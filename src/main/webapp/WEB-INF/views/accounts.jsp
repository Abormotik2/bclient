<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Accounts</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="<c:url value="/resources/css/main.css"/>" rel="stylesheet" type="text/css">
</head>
<body>

<form:form id="account" action="accounts" method="post" modelAttribute="account" >
<div class="main_header container-fluid"  style="height: 100px">
    <h2>${legalEntity.legalEntityFullName}</h2>
    <input class="btn btn-light" type="button" value="Назад"
           onclick="location.href = '/bclient/welcome';">
    <input class="btn btn-light" type="button" value="Открыть счет"
           onclick="document.getElementById('create_account_form').style.display = 'block';">
    <div class="error"><form:errors path="*"/></div>
</div>
<div class="create_panel container-fluid" id="create_account_form">

    <form:input class="create_input" id="account_name" name="account_name" path="accountName"
                placeholder="Название счета"/>
    <form:input class="create_input" id="account_num" name="account_num" path="accountNum" placeholder="Номер счета"/>
    <form:input class="create_input" id="currency_code" name="currency_code" path="currencyCode" placeholder="Валюта"/>
    <form:select class="create_select custom-select col-xs-2" path="bank.id">
        <form:options items="${bankList}" itemValue="id" itemLabel="bankName"/>
    </form:select>
    <form:input type="hidden" name="legal_entity" path="legalEntity.id" value="${legalEntity.id}"/><br>
    <input class="btn btn-light" type="button" onclick="document.getElementById('create_account_form').style.display = 'none';"
           value="Отмена">
    <input class="btn btn-light" type="submit" onclick="document.getElementById('create_account_form').style.display = 'none';"
           value="Сохранить">
    </form:form>
</div>
<div class="sort container-fluid">
    <span>Сортировка:</span>
    <div class="sort_select">
        <form:form id="sortAcc" action="accounts" method="get" modelAttribute="sortAcc" >
            <form:select class="custom-select col-xs-2" path="chooseType">
                <form:options items="${sortAcc.settingSort}" itemLabel="value"/>
            </form:select>
            <input type="submit" class="btn btn-light"/>
        </form:form>
    </div>
</div>
<div class="bankFilter container-fluid" align="left">
    <span>Фильтр банков:</span>
    <div class="sort_select">
        <form:form id="filter" action="accounts" method="get" modelAttribute="bankFilter">
            <form:select class="custom-select col-xs-2" path="bankName">
                <form:options items="${bankList}" itemLabel="bankName"/>
            </form:select>
            <input class = "btn btn-light" type="submit"/>
        </form:form>
    </div>
</div>
<br>
<div class="table">
    <table class="table-striped">
        <thead class="table-dark">
        <tr>
            <td>Название счета</td>
            <td>Номер счета</td>
            <td>Банк</td>
            <td>Баланс</td>
            <td>Валюта</td>
            <td>Статус</td>
        </tr>
        </thead>
        <c:forEach items="${availableAccounts}" var="acc">
            <tr>
                <td>${acc.accountName}</td>
                <td>${acc.accountNum}</td>
                <td>${acc.bank}</td>
                <td>${acc.balance.accountBalanceAmt}</td>
                <td>${acc.currencyCode}</td>
                <td>${acc.accountStatus.accountStatusName}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>