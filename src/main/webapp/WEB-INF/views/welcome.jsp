<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="<c:url value="/resources/css/main.css"/>" rel="stylesheet" type="text/css">
</head>
<body>
<div class="table table-dark">
    <h4 class="title">Личный кабинет</h4>
    <p class="welcome" align="right">Добро пожаловать ${name}!</p>
</div>
<div class="welcome_div">
    <h3>
        Выберите юридичесое лицо
    </h3>
    <form:form class="col-xs-3 container-fluid" id="legal_entity" action="welcome" method="post" modelAttribute="legalEntity">
        <form:select class="custom-select" path="id">
            <form:options items="${legalSet}" itemValue="id" itemLabel="legalEntityFullName"/>
        </form:select><br>
        <input class="btn btn-light" type="submit" name="param" value="Счета">
        <input class="btn btn-light" type="submit" name="param" value="Договора">
        <input class="btn btn-light" type="submit" name="param" value="Платежи">
        <input class="btn btn-light" type="submit" name="param" value="Выход">
    </form:form>
</div>
</body>
</html>
